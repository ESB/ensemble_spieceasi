# Network Analyses - SpiecEasi
- The Rscript and Snakefile herein can be used to run `SpiecEasi` for generating networks from taxonomic counts
- The SpiecEasi run is designed for Cross-domain (eg. 16S + 18S) interaction analyses
- The same can be applied for other interactions (eg. phages + bacteria etc.)

# Dependencies
- `snakemake >= 5.3.0`

# File format
- Matrix with the following features
- See files in the [data](https://git-r3lab.uni.lu/susheel.busi/spieceasi/-/tree/master/data) folder
    - rownames == samples
    - colnames == taxa
```
# Example
	ASV1	ASV2	ASV3	ASV4
Sample1	83	0	0	49
Sample2	0	0	0	0
Sample3	0	0	0	27
```

# How to run
- Provide a `list` of samples, eg. **`gfs_list`**
- Edit `line 17` in the `Snakefile` to match the name of the list
- To run the analyses, first activate `YOUR` snakemake environment and launch the jobs as follows:
```bash
conda activate snakemake
snakemake -s Snakefile --use-conda --cores 24 -rp
```

# References
- [SpiecEasi](https://github.com/zdk123/SpiecEasi)
- [Snakemake](https://github.com/snakemake/snakemake)
