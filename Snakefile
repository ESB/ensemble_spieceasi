"""
Author: Susheel Bhanu BUSI
Affiliation: Systems Ecology group LCSB UniLU
Date: [2021-08-03]
Run: snakemake -s Snakefile --use-conda --cores 96 -rp
Latest modification:
"""

import os
import glob

configfile:"config/config.yaml"
DATA_DIR=config['work_dir']
RESULTS_DIR=config['results_dir']
ENV_DIR=config['env_dir']
SRC_DIR=config['scripts_dir']
SAMPLES=[line.strip() for line in open("gfs_list", 'r')]    # if using a sample list instead of putting them in a config file


###########
rule all:
    input:
        expand(os.path.join(RESULTS_DIR, "RDS/{sample}.rds"), sample=SAMPLES)


#############
# SpiecEasi #
#############
rule spieceasi:
    input:
        PROK=os.path.join(DATA_DIR, "{sample}_TaraPhylaMtx.csv"),
        EUK=os.path.join(DATA_DIR, "{sample}_EUK_TaraPhylaMtx.csv"),
    output:
        NET=os.path.join(RESULTS_DIR, "RDS/{sample}.rds")
    log:
        os.path.join(RESULTS_DIR, "logs/{sample}.spieceasi.log")
    threads:
        config["threads"]
    benchmark:
        os.path.join(RESULTS_DIR, "benchmarks/{sample}.spieceasi.benchmark.txt")
    conda:
        os.path.join(ENV_DIR, "SpiecEasi.yaml")
    message:
        "Creating network for {wildcards.sample}"
    script:
        os.path.join(SRC_DIR, "glasso_speiceasi.R")
